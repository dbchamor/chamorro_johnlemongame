# README #

This game started off as a copy of Unity's John Lemon Tutorial. 
I made a level from scratch as a Puzzle/3D Platformer hybrid. 

### Controls ###

* Inverse Control (AWSD)
* Space Bar for Jump


### Object of the Game ###

* Collect 30 Lemons/Limes
* Best the Clock! You have 5 minutes
* Complete the Obstacle in Every Room
* Follow the Signs
* Have Fun!


### References ###

* ProBuilder/TextMeshPro
* Unity/ Slack/ Other Help Forums