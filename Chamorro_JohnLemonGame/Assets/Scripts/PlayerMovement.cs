﻿using System.Collections;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Threading;
using UnityEngine;
using TMPro;
using System.ComponentModel;

public class PlayerMovement : MonoBehaviour
{
    public float turnSpeed = 20f; //Adds the turnSpeed as a public member variable than can be tweaked in the inspector
    // '20f' is the starting direction the character will face

    Animator m_Animator; //Getting a reference to the Animator Component
    Vector3 m_Movement; //What GameObject's position is represented by PlayerMovement Class
    // PascalCase, variable that stores the movement for the character
    Quaternion m_Rotation = Quaternion.identity; //Way of storing rotations
    Rigidbody m_RigidBody; //Gets the rigidbody reference 
    AudioSource m_AudioSource;

    public TextMeshProUGUI countText; //adds a port under the JL player to tag the canvas
    public TextMeshProUGUI timeText;// adds a port under the JL player to tag the canvas
    public TextMeshProUGUI lifeText;

    public GameEnding gameEnding; //Refrences GameEnding script

    public int count; //creates count
    public int life = 3; //creates life
    public int currentLives; //keep track of current life

    public float timeRemaining = 60;
    public bool timerIsRunning = false;

    public Vector3 jump; //Controlls the 3 axis 
    public float jumpForce = 5.0f;
    public bool isGrounded; //Bool that checks if the player is on the ground or not


    // Start is called before the first frame update
    void Start()
    {
        m_Animator = GetComponent<Animator>(); //Calls the Animator Asset to the scene
        m_RigidBody = GetComponent<Rigidbody>(); //Calls the rigidbody to the scene
        m_AudioSource = GetComponent<AudioSource>(); //Calls the saved audiosource 

        
        count = 0; //sets initial count vaule
        SetCountText(); //Will send the count text to the UI to display

        timerIsRunning = true;

        //jump = new Vector3(0.0f, 5.0f, 0.0f);
        
        life = PlayerPrefs.GetInt("SavedLives", 3); // tells it to check saved lives and reset it to 3 when the player hits game over

        if(life <= 0)
        {
            life = 3;
        }
        SetLifeText(); // Will send the count text to the UI to display
    }

    void OnCollisionStay() //Checks if JL is on the ground before he can jump
    {
        isGrounded = true;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float horizontal = Input.GetAxis("Horizontal"); //Represents the A and D characters, 
                                                        //Decides whether the character should move left or right
        float vertical = Input.GetAxis("Vertical"); //Represents the W and S characters, 
                                                    //Decides whether the character should move up or down
        m_Movement.Set(horizontal, 0f, vertical); //Assigns a value to each of the sets
        m_Movement.Normalize(); //Calls a method on the vector itself, Normalizing it

        bool hasHorizontalInput = !Mathf.Approximately(horizontal, 0f); //Tells the animator component is character is walking left or right
        bool hasVerticleInput = !Mathf.Approximately(vertical, 0f); //Tells the animator component is character is walking up or down 
        bool isWalking = hasHorizontalInput || hasVerticleInput; //Combined both into a single bool 

        m_Animator.SetBool("IsWalking", isWalking); //Sets the isWalking Animator Parameter

        //timerIsRunning = PlayerMovement.timerIsRunning;

        if (isWalking)
        {
            if (!m_AudioSource.isPlaying) //Checks if the player is walking and then will play
            {
                m_AudioSource.Play(); //Plays music
            }
            else
            {
                m_AudioSource.Stop(); //Stops music
            }
        }

        Vector3 desiredForward = Vector3.RotateTowards(transform.forward, m_Movement, turnSpeed * Time.deltaTime, 0f);
        //Calculates the characters forward vector, allows for rotation

        m_Rotation = Quaternion.LookRotation(desiredForward); //This looks for the LookRotation method and creates a direction of the given parameter

    }

    void Update()
    {
        if (timerIsRunning)
        {
            if (timeRemaining > 0)
            {
                timeRemaining -= Time.deltaTime;
                DisplayTime(timeRemaining);
            }
            else
            {
                //Debug.Log("Time has run out!");
                timeRemaining = 0;
                timerIsRunning = false;
                gameEnding.CaughtPlayer();
            }
        }

        if (Input.GetKeyDown(KeyCode.Space) && isGrounded)
        {

            m_RigidBody.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);//supposed to add force to john lemon so that he jumps up
            isGrounded = false;
            print("Jumping");
        }
    }

    void OnTriggerEnter(Collider Collectible)
    {
        if (Collectible.gameObject.CompareTag("Collectible"))
        {
            Collectible.gameObject.SetActive(false);
            count = count + 1; //add one to count score when a Lemon is picked up
            SetCountText(); //If JL passes through a Lemon, it will update display count

        }
        else if (Collectible.gameObject.CompareTag("Collectible2"))
        {
            Collectible.gameObject.SetActive(false);
            count = count + 3;
            SetCountText();
            print("I picked up a Lime!");
        }
    }

    void OnCollisionEnter(Collision Floor) //When John Lemon hits the "Mirrored" Floor he will restart
    {
        if (Floor.gameObject.CompareTag("Floor"))
        {
            Floor.gameObject.SetActive(false);
            gameEnding.CaughtPlayer(); //Will display this if the player hit trigger
            //print("I restarted because you fell");
        }
    }

    public void LoseLife() 
    {
            life -= 1;
            PlayerPrefs.SetInt("SavedLives", life); //Save amount of lives to life int
            SetLifeText();
            //print("im working");

    } 

    void DisplayTime(float timeToDisplay)
    {
        timeToDisplay += 1;

        float minutes = Mathf.FloorToInt(timeToDisplay / 60);
        float seconds = Mathf.FloorToInt(timeToDisplay % 60);

        timeText.text = string.Format("{0:00}:{1:00}", minutes, seconds);
    }


    void SetCountText()
    {
        countText.text = "LemonCount: " + count.ToString(); //Keep an updated track to your UI screen
        //print("added 1!"); this line was used to test if the count was updating
    }

    void SetLifeText()
    {
        lifeText.text = "Lives: " + life.ToString(); //Will display if lives is triggered 
        //print("you died!"); just here to prove this was working
    }

    void OnAnimatorMove() //Allows us to apply root motion, means movement and rotation can be applied separately
    {
        m_RigidBody.MovePosition(m_RigidBody.position + m_Movement * m_Animator.deltaPosition.magnitude); //Combines the Movement and Rotation to the character
        m_RigidBody.MoveRotation(m_Rotation); //Applies to Rotation
        
    }
}