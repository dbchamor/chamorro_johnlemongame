﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using UnityEngine;

public class Observer : MonoBehaviour
{

    public Transform player; //Will access JohnLemons position to determine the clear line of sight
    public GameEnding gameEnding; //references gameending
    public PlayerMovement playerMovement; //refrences playermovement

    bool m_IsPlayerInRange; //Give a true or false if the player is in range of the gargoyle
    bool caught = false;

    void OnTriggerEnter (Collider other) //Triggers Gargoyle if John Lemon trips the trigger
    {
        if(other.transform == player)
        {
            m_IsPlayerInRange = true; //Checks the bool
        }
    }

    void OnTriggerExit (Collider other) //Checks if John Lemon is outside of the trigger
    {
        if(other.transform == player)
        {
            m_IsPlayerInRange = false; //Checks if bool is false
        }
    }

    void Update() // Checks the Enemys Line of Sight every frame
    {
        if (caught == true) // helps makes sure the player lives counter does not count every frame
        {
            return;
        }
        
        if (m_IsPlayerInRange) //Only checks line of sight if character is in the area
        {
            Vector3 direction = player.position - transform.position + Vector3.up; //creates direction 
            Ray ray = new Ray(transform.position, direction); //Creates new Ray

            RaycastHit raycastHit; //Defines the RaycastHit variable

            if (Physics.Raycast(ray, out raycastHit)) //creates a ray and checks if it has been hit
            {
                if(raycastHit.collider.transform == player)
                {
                    caught = true; //stops playermovement from being called every frame
                    playerMovement.LoseLife(); //calls the public lose life function in playermovement
                    gameEnding.CaughtPlayer();
                }
            }
        }
    }
}
