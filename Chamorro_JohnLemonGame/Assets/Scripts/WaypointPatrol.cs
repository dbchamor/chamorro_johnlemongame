﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI; //Tells the Nav Agent what destination is should have

public class WaypointPatrol : MonoBehaviour
{

    public NavMeshAgent navMeshAgent; //enables me to assign the Nav Agent in the inspector window
    public Transform[] waypoints; //Declares a public variable(waypoint) which is an array of transforms
    int m_CurrentWaypointIndex; //helps the ghost move onto the next waypoint after the last


    void Start()
    {
        navMeshAgent.SetDestination(waypoints[0].position);
    }

    
    void Update()
    {
        if(navMeshAgent.remainingDistance < navMeshAgent.stoppingDistance) //Checks if the Ghost has arrived at its destination
        {
            m_CurrentWaypointIndex = (m_CurrentWaypointIndex + 1) % waypoints.Length; //Gets the current index and finds the remainder
            navMeshAgent.SetDestination(waypoints[m_CurrentWaypointIndex].position); //Adds the remainder to the array 
        }
    }
}
