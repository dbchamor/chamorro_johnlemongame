﻿using System.Collections;
using System.Collections.Generic;
using System.Media;
using System.Runtime.Hosting;
using System.Runtime.InteropServices;
using System.Threading;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    public float fadeDuration = 1f; //allows for non-whole number, allows for the fade to happen over 1 sec
    public GameObject player; //makes sure fadeDuration only happens when John Lemon hits the trigger
    public float displayImageDuration = 1f; 

    public CanvasGroup exitBackgroundImageCanvasGroup; //Makes sure after the fade the game ends
    public AudioSource exitAudio; //Calls Escaped Audio
    public CanvasGroup caughtBackgroundImageCanvasGroup; //creates another canvas group so new images will display if caught
    public AudioSource caughtAudio; //Calls Caught Audio
    
    bool m_IsPlayerAtExit; //checks if player has reached the exit
    bool m_IsPlayerCaught; // checks if player is caught or not
    float m_Timer;// turns the time into a float
    bool m_HasAudioPlayed; //checks if the audio was player

    public PlayerMovement playerMovement; //Calls the PlayerMovement script
    
  
    //To detect the plater controlled GameObject
    void OnTriggerEnter (Collider other)
    {
        //Makes sure the ending is only triggered when JL hits the box
        if (other.gameObject == player) //uses equivalency operator returns true or false
        {
            PlayerMovement pMovement = player.GetComponent<PlayerMovement>();
            if (pMovement.count >= 30 && pMovement.timeRemaining > 0)
            {
                m_IsPlayerAtExit = true; //Triggers the bool
            }
        }
        
    }

    public void CaughtPlayer ()
    {
        m_IsPlayerCaught = true;
    }

    //Will call this function every frame
    void Update()
    {
        //PlayerMovement pMovement = player.GetComponent<playerMovement>();
        if (m_IsPlayerAtExit) //Check whether player has exited or and player has collected 5 lemons
        {
            //otherScript.GetComponent<PlayerMovement>().SetCountText(); //Should call SetCount from other script
            EndLevel(exitBackgroundImageCanvasGroup, false, exitAudio); //Calls the endlevel
        }
        else if (m_IsPlayerCaught) //Checkes if player was caught
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, caughtAudio);
        }
    }
    
    //It doesnt return anything and doesnt need parameters
    void EndLevel (CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource) 
    {
        if (!m_HasAudioPlayed)
        {
            audioSource.Play();
            m_HasAudioPlayed = true;
        }
        
        m_Timer += Time.deltaTime; //method that start counting up the timer
        imageCanvasGroup.alpha = m_Timer / fadeDuration; // Divides the timer by the duration

        //Quits the game when the fade is finished
        if (m_Timer > fadeDuration + displayImageDuration) //When time is greater than duration, the fade will finish
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0); //Reloads the scene
            }
            else
            {
                Application.Quit(); //Makes the game quit
            }
     
        }

    }

}
